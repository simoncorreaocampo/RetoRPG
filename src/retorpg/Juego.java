/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package retorpg;

import java.util.Random;
import javax.swing.JOptionPane;

/**
 *
 * @author simoncorrea
 */
public class Juego {
    private int puntos_vida_maximos = 5000;
    private int puntos_de_poder_maximos = 500;
    private int nivel = 5;
    private Nodo personajes = new Nodo();
    private Nodo enemigos = new Nodo();
    private int mundo = 1;
    private String[][] tablero = new String[8][8];

    public Juego() {
    }
    
    public void iniciar(){
        String nivel_str = JOptionPane.showInputDialog("¡¡Bienvenido a RetoRPG!! \nSelecciona un nivel de 1 a 5 para iniciar el juego");
        nivel = Integer.parseInt(nivel_str);
        System.out.println("--->Juego iniciado");
        crearPersonajes();
        crearTablero();
        empezarMundo();
    }
    
    private void crearPersonajes(){
        Random rand = new Random();
        Nodo auxiliar = personajes;
        for (int i = 0; i < 4; i ++){
            Personaje p = new Personaje(puntos_vida_maximos, puntos_de_poder_maximos);
            puntos_vida_maximos -= p.getSalud();
            auxiliar.setSiguiente(p);
            p.setAnterior(auxiliar);
            auxiliar = auxiliar.getSiguiente();
            boolean ubicacion_valida = false;
            while (ubicacion_valida == false){
                int x = 0;
                int y = rand.nextInt(6) + 2;
                if (tablero[x][y] == null){
                    tablero[x][y] = p.getId();
                    ubicacion_valida = true;
                    p.setX(x);
                    p.setY(y);
                }
            }
        }
        System.out.println("--->Personajes creados");
    }
    
    private void actualizarTablero(){
        System.out.println("--->Actualizando tablero");
        for (int i = 0; i < 8; i ++) {
            for (int j = 0; j < 8; j ++) {
                System.out.println("--->Tablero " + i + " - " + j);
                if (esEnemigo(j, i)){
                    tablero[j][i] = buscarEnemigo(j, i).getId();
                }
                else if (esPersonaje(j, i)){
                    tablero[j][i] = buscarPersonaje(j, i).getId();
                }
                else {
                    tablero[j][i] = null;
                }
            }
        }
        System.out.println("--->Tablero actualizado");
    }
    
    private void crearTablero(){
        Random rand = new Random();
        Nodo auxiliar = enemigos;
        for (int i = 0; i < mundo; i ++){
            Enemigo e = new Enemigo("E"+i, 1000*nivel, 50*nivel);
            boolean ubicacion_valida = false;
            auxiliar.setSiguiente(e);
            e.setAnterior(auxiliar);
            auxiliar = auxiliar.getSiguiente();
            while (ubicacion_valida == false){
                int x = rand.nextInt(6) + 2;
                int y = rand.nextInt(6) + 2;
                if (tablero[x][y] == null){
                    tablero[x][y] = e.getId();
                    ubicacion_valida = true;
                    e.setX(x);
                    e.setY(y);
                }
            }
        }
        System.out.println("--->Enemigos creados");
    }
    
    private void empezarMundo(){
        while (mundo < 10){
            
            JOptionPane.showMessageDialog(null, pintarTablero());
            
            System.out.println("--->Nuevo turno, matriz recorrida");
            turnos();
            
            if (contarEnemigos() == 0){
                System.out.println("--->Mundo completado");
                mundo ++;
                System.out.println("--->Mundo iniciado " + mundo);
            }
            if (contarPersonajes() == 0){
                System.out.println("--->GAME OVER");
                JOptionPane.showMessageDialog(null, "GAME OVER, llegaste hasta el mundo: " + mundo);
                System.exit(0);
            }
        }
        
    }
    
    private void turnos(){
        //Recorrer matris para mirar quien ataca a quien
        for (int i = 0; i < 8; i ++) {
            for (int j = 0; j < 8; j ++) {
                //El orden de turnos es la matris de izquierda a derecha y de arriba hacia abajo
                Random rand = new Random();
                
                int aX = j; //Original
                int aY = i; //Original
                int tX = j; //Objetivo
                int tY = i; //Objetivo
                
                int max_busquedas = 1;
                boolean puede_atacar = false;
                while (puede_atacar == false && max_busquedas <= 4){
                    
                    boolean puede_buscar = false;
                    while (puede_buscar == false){
                        int dir = rand.nextInt(4) + 1;
                        if (dir == 1 && tX - 1 >= 0){
                            //ATK IZQ
                            tX -= 1;
                            puede_buscar = true;
                        }
                        else if (dir == 2 && tY - 1 >= 0){
                            //ATK UP
                            tY -= 1;
                            puede_buscar = true;
                        }
                        else if (dir == 3 && tX + 1 <= 7){
                            //ATK DER
                            tX += 1;
                            puede_buscar = true;
                        }
                        else if (dir == 4 && tY + 1 <= 7){
                            //ATK DOWN
                            tY += 1;
                            puede_buscar = true;
                        }
                    }
                    

                    if (esEnemigo(aX, aY) && esPersonaje(tX, tY)){
                        puede_atacar = true;
                        enemigoVSPersonaje(buscarEnemigo(aX, aY), buscarPersonaje(tX, tY));
                    }
                    else if (esPersonaje(aX, aY) && esEnemigo(tX, tY)){
                        puede_atacar = true;
                        personajeVSEnemigo(buscarPersonaje(aX, aY), buscarEnemigo(tX, tY));
                    }
                    else{
                        max_busquedas ++;
                    }
                }
                
                if (esPersonaje(aX, aY) && max_busquedas >= 4){
                    moverPersonaje(buscarPersonaje(aX, aY));
                }
                
                actualizarTablero();
                pintarTablero();
                      
            }
        }
        
        //Todo personaje o enemigo ataca de manera aleatoria arriba, abajo, izquierda o derecha solo si tiene a quien atacar en esas
        //posiciones
        //El personaje no se puede mover si esta en rango de enemigo
    }
    
    private String pintarTablero(){
        String tablero_str = "|--|--|--|--|--|--|--|--|\n";
        for (int i = 0; i < 8; i ++) {
            for (int j = 0; j < 8; j ++) {
                if (j == 0){
                    tablero_str += "|";
                }
                if (tablero[j][i] != null){
                    tablero_str += tablero[j][i] + "|";
                }
                else{
                    tablero_str += "--|";
                }
                
                if (j == 7){
                    tablero_str += "\n";
                }
            }

        }
        System.out.println("--->Tablero pintado");
        return tablero_str + "\n\n";
    }
    
    private int contarEnemigos(){
        int cont = 0;
        for (int i = 0; i < 8; i ++) {
            for (int j = 0; j < 8; j ++) {
                if (esEnemigo(j, i)){
                    cont ++;
                }
            }
        }
        return cont;
    }
    
    private int contarPersonajes(){
        int cont = 0;
        for (int i = 0; i < 8; i ++) {
            for (int j = 0; j < 8; j ++) {
                if (esPersonaje(j, i)){
                    cont ++;
                }
            }
        }
        return cont;
    }
    
    private Enemigo buscarEnemigo(int x, int y){
        System.out.println("--->Buscando enemigo");
        Nodo auxiliar = enemigos;
        while (auxiliar.getSiguiente() != null){
            auxiliar = auxiliar.getSiguiente();
            Ubicacion ubi = (Ubicacion) auxiliar;
            if (ubi.getX() == x && ubi.getY() == y){
                System.out.println("--->Enemigo encontrado: " + x + " - " + y);
                return (Enemigo) auxiliar;
            }
        }
        System.out.println("--->Enemigo no encontrado");
        return null;
    }
    
    private Personaje buscarPersonaje(int x, int y){
        System.out.println("--->Buscando personaje");
        Nodo auxiliar = personajes;
        while (auxiliar.getSiguiente() != null){
            auxiliar = auxiliar.getSiguiente();
            Ubicacion ubi = (Ubicacion) auxiliar;
            if (ubi.getX() == x && ubi.getY() == y){
                System.out.println("--->Personaje encontrado: " + x + " - " + y);
                return (Personaje) auxiliar;
            }
        }
        System.out.println("--->Personaje no encontrado");
        return null;
    }
    
    private void matarPersonaje(Personaje personaje){
        Nodo auxiliar = personajes;
        while (auxiliar.getSiguiente() != null){
            auxiliar = auxiliar.getSiguiente();
            if (personaje.getId().equals(((Personaje) auxiliar).getId())){
                Nodo aux_anterior = auxiliar.getAnterior();
                Nodo aux_siguiente = auxiliar.getSiguiente();
                aux_anterior.setSiguiente(aux_siguiente);
                aux_siguiente.setAnterior(aux_anterior);
                System.out.println("--->Personaje matado: " + personaje.getId());
            }
        }
    }
    
    private void matarEnemigo(Enemigo enemigo){
        Nodo auxiliar = personajes;
        while (auxiliar.getSiguiente() != null){
            auxiliar = auxiliar.getSiguiente();
            if (enemigo.getId().equals(((Enemigo) auxiliar).getId())){
                Nodo aux_anterior = auxiliar.getAnterior();
                Nodo aux_siguiente = auxiliar.getSiguiente();
                aux_anterior.setSiguiente(aux_siguiente);
                aux_siguiente.setAnterior(aux_anterior);
                System.out.println("--->Enemigo matado: " + enemigo.getId());
            }
        }
    }
    
    private boolean esLibre(int x, int y){
        return tablero[x][y] == null;
    }
    
    private boolean esEnemigo(int x, int y){
        if (tablero[x][y] != null){
            return tablero[x][y].startsWith("E");
        }
        else{
            return false;
        }
    }
    
    private boolean esPersonaje(int x, int y){
        if (tablero[x][y] != null){
            return !tablero[x][y].startsWith("E");
        }
        else{
            return false;
        } 
    }
    
    private void personajeVSEnemigo(Personaje personaje, Enemigo enemigo){
        Poder poder = personaje.listarYSeleccionarPoder();
        System.out.println("---->Salud Enemigo: " + enemigo.getSalud());
        System.out.println("---->Daño Poder: " + poder.getDano());
        int salud = enemigo.getSalud() - poder.getDano();
        enemigo.setSalud(salud);
        if (salud <= 0){
            int puntos = personaje.getPunto_acum() + enemigo.getSalud()/100;
            personaje.setPunto_acum(puntos);
            personaje.calcularNivel();
            matarEnemigo(enemigo);
        }
    }
    
    private void enemigoVSPersonaje(Enemigo enemigo, Personaje personaje){
        int salud = personaje.getSalud() - enemigo.getDano();
        personaje.setSalud(salud);
        if (salud <= 0){
            matarPersonaje(personaje);
        }
    }
    
    private void moverPersonaje(Personaje personaje){
        
        int aX = personaje.getX(); //Original
        int aY = personaje.getY(); //Original
        int tX = personaje.getX(); //Objetivo
        int tY = personaje.getY(); //Objetivo
        
        boolean puede_mover = false;
        while (puede_mover == false){
            String dir = JOptionPane.showInputDialog(pintarTablero()+"Mueve al personaje: " + personaje.getId() + ".\nW = Arriba\nS = Abajo\nA = Izquierda\nD = Derecha\nQ = No Mover");
            
                        
            if (dir.equals("A") && personaje.getX() - 1 >= 0 && esLibre(tX - 1, tY)){
                //ATK IZQ
                puede_mover = true;
                tX -= 1;
            }
            else if (dir.equals("W") && personaje.getY() - 1 >= 0 && esLibre(tX, tY - 1)){
                //ATK UP
                puede_mover = true;
                tY -= 1;
            }
            else if (dir.equals("D") && personaje.getX() + 1 <= 7 && esLibre(tX + 1, tY)){
                //ATK DER
                puede_mover = true;
                tX += 1;
            }
            else if (dir.equals("S") && personaje.getY() + 1 <= 7 && esLibre(tX, tY + 1)){
                //ATK DOWN
                puede_mover = true;
                tY += 1;
            }
            else if (dir.equals("Q")){
                //ATK DOWN
                puede_mover = true;
            }
            else{
                JOptionPane.showMessageDialog(null, pintarTablero()+"El personaje no se puede mover a esa posición");
            }
        }
        tablero[aX][aY] = null;
        tablero[tX][tY] = personaje.getId();
        personaje.setX(tX);
        personaje.setY(tY);
        
    }
    
    
    
    
}
