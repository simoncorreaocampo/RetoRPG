/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package retorpg;

import javax.swing.JOptionPane;

/**
 *
 * @author simoncorrea
 */
public class Poder extends Nodo {
    private String nombre;
    private int dano = 0;
    private int usos;

    public Poder(int poder_disponible) {
        this.nombre = JOptionPane.showInputDialog("Escoje un nombre para tu poder");
        while (this.dano == 0){
          String dano_str = JOptionPane.showInputDialog("Tienes " + poder_disponible + " para usar en este poder. \nValor de daó del poder"); 
          int aux_dano = Integer.parseInt(dano_str);
          if (aux_dano > 0 && aux_dano <= poder_disponible){
              this.dano = aux_dano;
              this.usos = 1000/this.dano;
          }
        } 
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getDano() {
        return dano;
    }

    public void setDano(int dano) {
        this.dano = dano;
    }

    public int getUsos() {
        return usos;
    }

    public void setUsos(int usos) {
        this.usos = usos;
    }
    
    
}
