/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package retorpg;

/**
 *
 * @author simoncorrea
 */
public class Enemigo extends Ubicacion{
    private String id;
    private int salud;
    private int dano;

    public Enemigo(String id, int salud, int dano) {
        this.id = id;
        this.salud = salud;
        this.dano = dano;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getSalud() {
        return salud;
    }

    public void setSalud(int salud) {
        this.salud = salud;
    }

    public int getDano() {
        return dano;
    }

    public void setDano(int dano) {
        this.dano = dano;
    }
    
    
}
