/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package retorpg;

import javax.swing.JOptionPane;

/**
 *
 * @author simoncorrea
 */
public class Personaje extends Ubicacion {
    private String id;
    private int nivel = 1;
    private int salud = 0;
    private int punto_acum = 0;
    private int next_level = 0;
    private int factor_de_nivel;
    private int poder_maximo;
    private Nodo poderes = new Nodo();

    public Personaje(int vida_disponible, int poder_maximo) {
        String aux = "";
        while (aux.length() != 2){
            aux = JOptionPane.showInputDialog("Escoje una letra para tu personaje. EJE: W, X, Y, Z");
            if (aux.length() != 2){
                JOptionPane.showMessageDialog(null, "El nombre del personaje debe ser de 2 letras.");
            }
            else{
                this.id = aux;
            }
        }
        
        while (this.salud == 0){
          String salud_str = JOptionPane.showInputDialog("Tienes " + vida_disponible + " para usar en este personaje. \nValor de Salud del personaje"); 
          int aux_salud = Integer.parseInt(salud_str);
          if (aux_salud > 0 && aux_salud <= vida_disponible){
              this.salud = aux_salud;
              this.factor_de_nivel = this.salud%100;
              this.poder_maximo = poder_maximo;
          }
        } 
        
        Nodo auxiliar = poderes;
        for (int i = 0; i < 4; i ++){
            Poder po = new Poder(this.poder_maximo);
            this.poder_maximo -= po.getDano();
            auxiliar.setSiguiente(po);
            po.setAnterior(auxiliar);
            auxiliar = auxiliar.getSiguiente();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getSalud() {
        return salud;
    }

    public void setSalud(int salud) {
        this.salud = salud;
    }

    public int getPunto_acum() {
        return punto_acum;
    }

    public void setPunto_acum(int punto_acum) {
        this.punto_acum = punto_acum;
    }

    public int getNext_level() {
        return next_level;
    }

    public void setNext_level(int next_level) {
        this.next_level = next_level;
    }

    public int getFactor_de_nivel() {
        return factor_de_nivel;
    }

    public void setFactor_de_nivel(int factor_de_nivel) {
        this.factor_de_nivel = factor_de_nivel;
    }

    public int getPoder_maximo() {
        return poder_maximo;
    }

    public void setPoder_maximo(int poder_maximo) {
        this.poder_maximo = poder_maximo;
    }
    
    public Poder listarYSeleccionarPoder(){
        String poderes_str = "";
        
        Nodo auxiliar = this.poderes;
        int i = 1;
        while (auxiliar.getSiguiente() != null){
            auxiliar = auxiliar.getSiguiente();
            Poder poder = (Poder) auxiliar;
            if (poder.getUsos() > 0){
                poderes_str += "\n" + i + ": " + poder.getNombre() + " - DX: " + poder.getDano() + " - UX: " + poder.getUsos();
            }
            i ++;
        }
        
        int opcion = Integer.parseInt(JOptionPane.showInputDialog("Selecciona un poder del jugador:\nID: " + this.id + "\nSALUD: " + this.salud + "\nNIVEL: " 
        +this.nivel + "\n\nPODERES: " + poderes_str));
        
        i = 1;
        auxiliar = this.poderes;
        while (auxiliar.getSiguiente() != null){
            auxiliar = auxiliar.getSiguiente();
            if (i == opcion){
                Poder p = (Poder) auxiliar;
                int usos = p.getUsos() - 1;
                p.setUsos(usos);
                return p;
            }
            i ++;
        }
        
        return null;
    }
    
    public void calcularNivel(){
        int siguiente_nivel = this.nivel * (10 * this.factor_de_nivel);
        if (this.punto_acum > siguiente_nivel){
            this.nivel ++;
        }
    }
    
    
}
